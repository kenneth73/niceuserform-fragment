package com.example.nice_user_from_fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class RegisterFragment extends Fragment implements View.OnClickListener {
    String[] SPINNERLIST = {"Male", "Female"};

    private TextInputLayout usernameLayout;
    private TextInputEditText usernameInput; //required
    private TextInputLayout passwordLayout;
    private TextInputEditText passwordInput; //required
    private TextInputLayout repeatPasswordLayout;
    private TextInputEditText repeatPasswordInput; //required

    private TextInputLayout emailLayout;
    private TextInputEditText emailInput; //required

    private TextInputLayout nameLayout;
    private TextInputEditText nameInput;

    private TextInputLayout surnameLayout;
    private TextInputEditText surnameInput;

    private Button pickDateBtn;

    //final MaterialDatePicker materialDatePicker ;

    private CheckBox aceptTermsCheckbox;
    private TextView aceptTermsTextView;

    Button registerBtn;
    Button loginBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment, container, false);
        //layouts
        usernameLayout = view.findViewById(R.id.username_reg_layout);
        passwordLayout = view.findViewById(R.id.password_reg_layout);
        repeatPasswordLayout = view.findViewById(R.id.password_repeat_reg_layout);
        emailLayout = view.findViewById(R.id.email_reg_layout);
        nameLayout = view.findViewById(R.id.name_reg_layout);
        surnameLayout = view.findViewById(R.id.surname_reg_layout);
        //inputs
        usernameInput = view.findViewById(R.id.username_reg_input);
        passwordInput = view.findViewById(R.id.password_reg_input);
        repeatPasswordInput = view.findViewById(R.id.password_repeat_reg_input);
        emailInput = view.findViewById(R.id.email_reg_input);
        nameInput = view.findViewById(R.id.name_reg_input);
        surnameInput = view.findViewById(R.id.surname_reg_input);
        //checkbox
        aceptTermsCheckbox = view.findViewById(R.id.accept_terms_checkbox);
        aceptTermsTextView = view.findViewById(R.id.message_terms_textView);
        //buttons
        pickDateBtn = view.findViewById(R.id.pick_date_button);
        registerBtn = view.findViewById(R.id.goto_register_welcome_btn);
        loginBtn = view.findViewById(R.id.goto_register_login_btn);
        registerBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);

        usernameLayout.setAlpha((float) 0.7);
        passwordLayout.setAlpha((float) 0.7);
        repeatPasswordLayout.setAlpha((float) 0.7);
        emailLayout.setAlpha((float) 0.7);
        nameLayout.setAlpha((float) 0.7);
        surnameLayout.setAlpha((float) 0.7);
        aceptTermsCheckbox.setAlpha((float) 0.7);
        pickDateBtn.setAlpha((float) 0.7);
        loginBtn.setAlpha((float) 0.5);
        registerBtn.setAlpha((float) 1);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        MaterialBetterSpinner materialDesignSpinner = (MaterialBetterSpinner) view.findViewById(R.id.gender_spinner);
        materialDesignSpinner.setAdapter(arrayAdapter);

        MaterialDatePicker.Builder materialDateBuilder = MaterialDatePicker.Builder.datePicker();

        materialDateBuilder.setTitleText("SELECT BIRTHDATE");

        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();
        materialDatePicker.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        pickDateBtn.setText("Birth date: " + materialDatePicker.getHeaderText());
                    }
                });

        pickDateBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDatePicker.show(getActivity().getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
                    }
                });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goto_register_welcome_btn:
                boolean termsConditions = aceptTermsCheckbox.isChecked();
                boolean checkFields = true;
                String user = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                String password2 = repeatPasswordInput.getText().toString();
                String email = emailInput.getText().toString();

                if (termsConditions) {
                    aceptTermsTextView.setVisibility(View.INVISIBLE);
                } else {
                    aceptTermsTextView.setVisibility(View.VISIBLE);
                    aceptTermsTextView.setText("You have to accept the conditions");
                    checkFields=false;
                }

                if (user.length() < 1) {
                    usernameLayout.setError("username is required");
                    checkFields = false;
                } else {
                    usernameLayout.setError("");
                }
                if (password.length() < 8) {
                    passwordLayout.setError("Minimum 8 characters");
                    checkFields = false;
                } else {
                    passwordLayout.setError("");
                }

                if (email.length() < 1) {
                    emailLayout.setError("email is required");
                    checkFields = false;
                } else {
                    emailLayout.setError("");
                }
                if (!password.equalsIgnoreCase(password2)) {
                    repeatPasswordLayout.setError("Passwords do not match");
                    checkFields = false;
                } else {
                    repeatPasswordLayout.setError("");
                }
                if (checkFields) {
                    Navigation.findNavController(v).navigate(R.id.action_registerFragment_to_welcomeFragment);
                }

                break;
            case R.id.goto_register_login_btn:
                Navigation.findNavController(v).navigate(R.id.action_registerFragment_to_loginFragment);
                break;
        }
    }
}
