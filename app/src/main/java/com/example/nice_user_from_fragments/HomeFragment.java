package com.example.nice_user_from_fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

public class HomeFragment extends Fragment implements View.OnClickListener {

    Button registerBtn;
    Button loginBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        registerBtn = view.findViewById(R.id.btn_register);
        loginBtn = view.findViewById(R.id.btn_login);
        registerBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_registerFragment);
                break;
            case R.id.btn_login:
                Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_loginFragment);
                break;
        }
    }
}
