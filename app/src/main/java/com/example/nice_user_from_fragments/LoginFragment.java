package com.example.nice_user_from_fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginFragment extends Fragment implements View.OnClickListener {
    private TextInputLayout usernameLayout;
    private TextInputEditText usernameInput;
    private TextInputLayout passwordLayout;
    private TextInputEditText passwordInput;

    Button registerBtn;
    Button loginBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        registerBtn = view.findViewById(R.id.goto_login_register_btn);
        loginBtn = view.findViewById(R.id.goto_login_welcome_btn);
        registerBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);

        usernameInput = view.findViewById(R.id.username_input);
        usernameLayout = view.findViewById(R.id.username_layout);
        passwordInput = view.findViewById(R.id.password_input);
        passwordLayout = view.findViewById(R.id.password_layout);

        usernameLayout.setAlpha((float) 0.7);
        passwordLayout.setAlpha((float) 0.7);
        usernameInput.setAlpha((float)0.7);
        passwordInput.setAlpha((float) 0.7);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goto_login_register_btn:
                Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_registerFragment);
                break;
            case R.id.goto_login_welcome_btn:
                String user= usernameInput.getText().toString();
                String password= passwordInput.getText().toString();
                if (user.length()>=1 && password.length()>7) {
                    Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_welcomeFragment);

                } else {
                    if (user.length() == 0) usernameLayout.setError("username is required");
                    else usernameLayout.setError("");

                    if (password.length()<8) passwordLayout.setError("Minimum 8 characters");
                    else passwordLayout.setError("");
                }
                break;
        }
    }
}
